package org.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.example.dao.User;

import java.util.List;

@Mapper
public interface UserXmlMapper {
    List<User> findAll();
}