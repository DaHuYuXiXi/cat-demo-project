package org.example.dao;

import lombok.Data;

@Data
public class User {
    private Integer id;
    private String username;
    private Integer password;
}
