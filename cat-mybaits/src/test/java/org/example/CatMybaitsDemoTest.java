package org.example;

import org.example.mapper.UserXmlMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest(classes = CatMybaitsDemo.class)
public class CatMybaitsDemoTest {
    @Resource
    private UserXmlMapper userXmlMapper;

    @Test
    public void testSearchUser() throws InterruptedException {
        try {
            userXmlMapper.findAll().forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Thread.sleep(30000);
    }
}
