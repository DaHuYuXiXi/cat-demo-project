package org.example;

import com.dianping.cat.Cat;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.logging.Logger;


@SpringBootTest(classes = CatDemoMain.class)
@Slf4j
public class CatLogBackDemoTest {

    @Test
    public void testLog() throws InterruptedException {
        //需要开启跟踪模式,才会生效--可以参考CatLogbackAppender的append方法,一看便知
        Cat.getManager().setTraceMode(true);
        log.info("cat info");
        try {
            int i = 1/0;
        }catch (Exception e){
            log.error("cat error",e);
        }
        //睡眠一会,让cat客户端有时间上报错误
        Thread.sleep(100000);
    }
}
