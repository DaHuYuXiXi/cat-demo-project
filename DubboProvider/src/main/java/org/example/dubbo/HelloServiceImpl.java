package org.example.dubbo;

import org.apache.dubbo.config.annotation.DubboService;

@DubboService(interfaceClass = HelloService.class)
public class HelloServiceImpl implements HelloService {

    public String hello() {
        return "hello cat";
    }
}