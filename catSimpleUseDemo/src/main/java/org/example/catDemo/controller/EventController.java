package org.example.catDemo.controller;


import com.dianping.cat.Cat;
import com.dianping.cat.message.Event;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/event")
public class EventController {
    @RequestMapping("/logEvent")
    public String logEvent(){
        Cat.logEvent("URL.Server", "serverIp",
                Event.SUCCESS, "ip=127.0.0.1");
        return "test";
    }

    @RequestMapping("/logError")
    public String logError(){
        try {
            int i = 1 / 0;
        } catch (Throwable e) {
            Cat.logError("error(X) := exception(X)", e);
        }
        return "test";
    }
}
