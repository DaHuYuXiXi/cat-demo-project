
package org.example.catDemo.controller;

import com.dianping.cat.Cat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/metric")
public class MetricController {

    @RequestMapping("/count")
    public String count(){
        Cat.logMetricForCount("count");
        return "test";
    }

    @RequestMapping("/duration")
    public String duration(){
        Cat.logMetricForDuration("duration", 1000);
        return "test";
    }
}