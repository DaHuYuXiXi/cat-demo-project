package org.example.catDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatDemoMain {
    public static void main(String[] args) {
        SpringApplication.run(CatDemoMain.class,args);
    }
}
