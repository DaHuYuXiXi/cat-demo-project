package org.example;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDubbo(scanBasePackages = "org.example.dubbo")
@SpringBootApplication
public class CatConsumerDemo {
    public static void main(String[] args) {
        SpringApplication.run(CatConsumerDemo.class,args);
    }
}
