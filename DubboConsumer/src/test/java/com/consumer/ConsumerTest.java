package com.consumer;

import org.apache.dubbo.config.annotation.DubboReference;
import org.example.CatConsumerDemo;
import org.example.dubbo.HelloService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = CatConsumerDemo.class)
public class ConsumerTest {
    @DubboReference(url = "dubbo://127.0.0.1:20880")
    private HelloService helloService;

    @Test
    public void test(){
        for (int i = 0; i < 1000; i++) {
            System.out.println(helloService.hello());
        }
    }
}
